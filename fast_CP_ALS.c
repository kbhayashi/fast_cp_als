#include "tensors.h"
#include "dim_trees.h"

void in_order_reuse_MTTKRP( tensor * T, ktensor * Y, int s, int n, tensor * projection_Tensor, tensor * buffer_Tensor, ktensor * projection_Ktensor, int num_threads ){

	mkl_set_num_threads(num_threads);

	direction D;

	if( n == 0 ){
		/**
			Updating the first factor matrix, do a partial MTTKRP
		*/
		D = right; // 	Contracting over the right modes
		LR_Ktensor_Reordering_existingY( Y, projection_Ktensor, s + 1, opposite_direction(D) );	// s+1 because s is included in the left

		if( projection_Ktensor->nmodes == 1 ){	// factor matrix output PM
			/**
				PM is a factor matrix update
				D) right, tells the function that 0 is being updated
				Y) the original ktensor
				T) the original data tensor
				num_threads)
			*/
			partial_MTTKRP_with_KRP_output_FM( D, Y, T, num_threads );
		}
		else{					// tensor output PM
			/**
				PM outputs an intermediate tensor
				T) the original tensor, PM always takes the original tensor
				Y) the original ktensor, 
				D)
				s) overall split point
				projection_Tensor)
				num_threads)
			*/
			partial_MTTKRP_with_KRP_output_T( s, D, Y, T, projection_Tensor, num_threads );
			multi_TTV_with_KRP_output_FM( D, projection_Tensor, projection_Ktensor, num_threads );
		}
	}
	else if( n == s + 1 ){
		/**
			Updating the first left side, do a partial MTTKRP
		*/
		D = left; //	Contracting over the left modes
		LR_Ktensor_Reordering_existingY( Y, projection_Ktensor, s, opposite_direction(D) ); // s because s is not included on the right
		if( projection_Ktensor->nmodes == 1 ){	// factor matrix output PM
			partial_MTTKRP_with_KRP_output_FM( D, Y, T, num_threads );
		}
		else{
			partial_MTTKRP_with_KRP_output_T( s, D, Y, T, projection_Tensor, num_threads );

			// op(D) because multi ttvs in this tree structure are always right
			multi_TTV_with_KRP_output_FM( opposite_direction(D), projection_Tensor, projection_Ktensor, num_threads );
		}
	}
	else{
		D = left;
		if( projection_Ktensor->nmodes == 2 ){
			// A single left contraction to output a factor martix
			multi_TTV_with_KRP_output_FM( D, projection_Tensor, projection_Ktensor, num_threads );
		}
		else{
			/**
				1) A single left contraction to updage the projection_tensor
				2) A right contraction to get the desired MTTKRP
			*/
			multi_TTV_with_KRP_output_T( 0, D, projection_Tensor, projection_Ktensor, buffer_Tensor, num_threads );

			LR_tensor_Reduction( buffer_Tensor, projection_Tensor, buffer_Tensor->nmodes, D );
			tensor_data_swap( projection_Tensor, buffer_Tensor );

			remove_mode_Ktensor( projection_Ktensor, 0 );// remove the 0th factor matrix and mode from the projection_Ktensor
			multi_TTV_with_KRP_output_FM( right, projection_Tensor, projection_Ktensor, num_threads );
		}
	}

}

/**

	Implements the a dimension tree similar to the one proposed by Phan et. al
	Except both sides of this tree operate in the same way, instead of in
	an "opposite" way.

	1) T, tensor to be decomposed
	2) Y, T's cp model
	3) s, the split dimension
	4) eps, approximation tolerance
	5) max_iters
	6) num_threads
*/
void reuse_CP_ALS( tensor * T, ktensor * Y, int s, double eps, int max_iters, int num_threads){

	mkl_set_num_threads(num_threads);

	tensor 	projection_Tensor;	// Tensor for saving the contracted tensors
	tensor 	buffer_Tensor;		// Tensor for when the projection tensor is operated on
	ktensor projection_Ktensor;	// Ktensor for keeping track of factor matrices as we move down the tree

	int i, j, n, iter, min_dim, max_dim, ldp, rdp, in_debug_mode;
	double tensor_norm , e1, e2;
	
	char uplo = 'L';		// input for dposv lapack function
	int info = -1000;		// output for dposv lapack function

	in_debug_mode = 0;		// 1 means in debug mode, 0 means NOT in debug mode

	if( eps == -1 )
		eps = 2.220446049250313E-16;
	e1 = 1.0;

	tensor_norm = cblas_dnrm2( Y->dims_product, T->data, 1 ); 

	/**
		Ordering the intputs correctly
		AND
		Choosing s are preprocessing steps
	*/
	for( i = 0, ldp = 1; i < s+1; i++ )
		ldp *= T->dims[i];
	for( i = s+1, rdp = 1; i < T->nmodes; i++ )
		rdp *= T->dims[i];

	//	Compute the max and min dimension index
	min_dim = 0;
	max_dim = 0;
	for( i = 0; i < Y->nmodes; i++ ){
		if( Y->dims[i] < Y->dims[min_dim] )
			min_dim = i;
		if( Y->dims[i] > Y->dims[max_dim] )
			max_dim = i;
	}

	/**
		Allocate memory
		H) 		Multiple Hadamards Product
		H_copy)		Copy of V for computing the fit
		SYRKs) 		The array of SYRKs of the factor matrices
		error_factor)	Extra factor matrix for computing the model fit when n == N-1
		X)		Extra tensor for error checking
		KRP_check) 	Space for a Full Khatri Rao Product, allocate enough memory for the largest one, compute the smallest dimension first, for checking correctness of MTTKRP
		temp_factor)	factor for checking the MTTKRP correctness
	*/
	double * H, * KRP_check, * X, ** SYRKs, * temp_factor, * error_factor, * H_copy;
	H 		= (double*)malloc( sizeof(double)*Y->rank*Y->rank );
	H_copy 		= (double*)malloc( sizeof(double)*Y->rank*Y->rank );
	SYRKs 		= (double**)malloc( sizeof(double*)*T->nmodes );
	error_factor 	= (double*)malloc( sizeof(double)*Y->dims[T->nmodes-1]*Y->rank );

	if( in_debug_mode == 1 ){
		X 		= (double*)malloc( sizeof(double)*Y->dims_product );
		KRP_check 	= (double*)malloc( sizeof(double)*Y->rank*(Y->dims_product/Y->dims[min_dim]) );
		temp_factor 	= (double*)malloc( sizeof(double)*Y->dims[max_dim]*Y->rank );
	}

	for( i = 0; i < T->nmodes; i++ )
		SYRKs[i] = (double*)malloc( sizeof(double)*Y->rank*Y->rank );

	// Allocate memory for the larger of two partial MTTKRP
	if( ldp <= rdp ){
		projection_Tensor.data = (double*)malloc( sizeof(double) * rdp * Y->rank );
		buffer_Tensor.data = (double*)malloc( sizeof(double) * Y->rank * rdp );
	}
	else{
		projection_Tensor.data = (double*)malloc( sizeof(double) * ldp * Y->rank );
		buffer_Tensor.data = (double*)malloc( sizeof(double) * Y->rank * ldp );
	}

	projection_Tensor.nmodes 	= T->nmodes;
	buffer_Tensor.nmodes 		= T->nmodes;

	projection_Tensor.dims 	= (int*)malloc(sizeof(int) * T->nmodes);
	buffer_Tensor.dims 	= (int*)malloc(sizeof(int) * T->nmodes);
	for( i = 0; i < T->nmodes; i++ ){
		projection_Tensor.dims[i]	= T->dims[i];
		buffer_Tensor.dims[i]	 	= T->dims[i];
	}

	projection_Tensor.dims_product 	= T->dims_product;
	buffer_Tensor.dims_product 	= T->dims_product;

	ktensor_copy_constructor( Y, &projection_Ktensor );

	srand(time(NULL));
	for( i = 0; i < Y->nmodes; i++ ){
		for( j = 0; j < Y->rank * Y->dims[i]; j++){
			Y->factors[i][j] = (((double)rand()/(double)RAND_MAX) - 0.5)*2.0;
		}
	}
	
	if( in_debug_mode == 1){
		printf("--------Starting Factor Matrices--------\n");
		for( i = 0; i < Y->nmodes; i++ ){
			printf("Y.factors[%d]", i);
			printM_RowMajor(Y->factors[i], Y->rank, Y->dims[i]);
		}
		printf("----------------------------------------\n");
	}

	normalize_Ktensor_RowMajor( Y );

	if( in_debug_mode == 1){
		printf("--------Starting Factor Matrices_normalized--------\n");
		for( i = 0; i < Y->nmodes; i++ ){
			printf("Y.factors[%d]", i);
			printM_RowMajor(Y->factors[i], Y->rank, Y->dims[i]);
		}
		printf("----------------------------------------\n");
	}

	do_SYRKs_RowMajor( Y, SYRKs );		// Compute all the Symmetric factor Matrices

	for( iter = 0; iter < max_iters; iter++ ){
		for( n = 0; n < T->nmodes; n++ ){

			MHada_RowMajor( Y, SYRKs, H, n);	// Computer H

			in_order_reuse_MTTKRP( T, Y, s, n, &projection_Tensor, &buffer_Tensor, &projection_Ktensor, num_threads );

			// do MTTKRP
			if( in_debug_mode == 1){		
				Multi_KR_RowMajor( Y, KRP_check, n);
				MTTKRP_RowMajor( T, KRP_check, temp_factor, Y->rank, n);
				printf("********* MTTKRP check\nn = %d", n);
				printM_RowMajor(Y->factors[n], Y->rank, Y->dims[n]);
				printM_RowMajor(temp_factor, Y->rank, Y->dims[n]);	
				CompareM( Y->factors[n], temp_factor, Y->dims[n], Y->rank, -1.0 );
				printf("END *********MTTKRP check\n");
			}

			// Do solve
			if( n == T->nmodes-1 ){	// On the last iteration make a copy of MTTKRP and V so we can check the error fit
				cblas_dcopy( Y->rank*Y->dims[n], Y->factors[n], 1, error_factor, 1 );
				cblas_dcopy( Y->rank*Y->rank, H, 1, H_copy, 1 );
			}

			info = LAPACKE_dposv(LAPACK_COL_MAJOR, uplo, Y->rank, Y->dims[n], H, Y->rank, Y->factors[n], Y->rank);
			if( info != 0 ){
				printf("fuction LAPACKE_dposv in reuse_ALS_RowMajor(), returned %d\n. Exiting.", info);
				exit(-1);
			}
			
			/**
				Compute the current approximation error on the last mode update.
			*/
			if( n == T->nmodes-1 ){
				if( in_debug_mode == 1){
					e1 = CP_ALS_naive_error_computation( T, Y, X, KRP_check, n );
					printf("ITER=%d,n=%d,e1=%.16lf\n", iter, n, e1/tensor_norm);
				}		

				e2 = CP_ALS_efficient_error_computation( Y, n, error_factor, H_copy, SYRKs[n], tensor_norm );

				if( e2 <= 0.0 ){
					printf("ITER=%d, e2=%.16lf\n",iter, e2);
				}
				else{
					printf("ITER=%d , sqrt(e2)/norm_2(X)=%.16lf\n",iter, sqrt(e2)/tensor_norm);
				}

				if( e2 != -2.0 && e2 < eps ){
					printf("eps = %.16lf and e2 = %.16lf, BREAK! on ITER = %d\n", eps, e2, iter);
					iter = max_iters;
				}
			}
			else{

				normalize_Factor_Matrix_RowMajor(Y, n);
			
				cblas_dsyrk( CblasRowMajor, CblasUpper, CblasTrans, Y->rank, Y->dims[n], 1.0, Y->factors[n], Y->rank, 0.0, SYRKs[n], Y->rank );
			}

		}	// End CP_ALS inner loop

	}	// End CP_ALS outer loop

	free( H );
	free( H_copy );
	for( i = 0; i < T->nmodes; i++ )
		free(SYRKs[i]);
	free( SYRKs );
	free( error_factor );

	destruct_Tensor( &projection_Tensor );
	destruct_Tensor( &buffer_Tensor );	

	destruct_Ktensor( &projection_Ktensor, 0 );

	if( in_debug_mode == 1 ){
		free( X );
		free( KRP_check );
		free( temp_factor );
	}
}

/**
	Inputs:
	rank, noise, num_threads, max_iters, eps, N, dims, s
	1) rank, desired rank of the generated tensor
	2) num_threads, number of threads to be used
	3) max_iters, maximum number of iteration that CP_ALS will run
	4) eps, the desired tolerance of the approximation, -1 will give machine eps and -2 will force it to reach max_iters
	5) N, the number of modes in the desired tensor
	6) s, the split point
*/
int main( int argc, char *argv[] ){

	tensor_inputs inputs;

	process_inputs( argc, argv, &inputs );

	tensor T;
	ktensor Y;

	/**
		s)		split point
		als_start)	Timing variable
		als_end)	Timing variable
	*/
	int s;
	double als_start, als_end;

	s = atoi(argv[argc-1]);

	Gen_Tensor( &Y, &T, inputs.rank, inputs.nmodes, inputs.dims, 0.0 );// Generate a low rank tensor and ktensor
	printf("T.nmodes = %d\n", T.nmodes);

	als_start = omp_get_wtime();
	reuse_CP_ALS( &T, &Y, s, inputs.tolerance, inputs.max_iters, inputs.num_threads );
	als_end = omp_get_wtime() - als_start;
	
	//print_Ktensor_RowMajor( &Y );

	printf("TIME = %lf\n", als_end);

	destroy_inputs( &inputs );
	clean_Up_Gen_Tensor( &Y, &T );
	
	printf("EL FIN\n");

	return 0;
	exit(0);
}
