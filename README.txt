This repo has an implementation of cp_fastals.m from TENSORBOX and described in this paper.
https://arxiv.org/pdf/1204.1586.pdf

What is a CP decomposition?
For those unfamiliar with the CP it can be thought of as similar to the Singular Value Decomposition (SVD) but for high
order tensors (3 or more dimensions).
An intro to tensors and the CP Decomosition can be found here:
http://epubs.siam.org/doi/pdf/10.1137/07070111X

This repo does not provide support for running CP on an input tensor. This functionality will be added later either in C or through MATLAB. It would not be difficult to implement this on one’s own if so desired.

You can run a sample of the code through using the fast_CP_ALS.c file.

make –f makefile.par 
./fast_CP_ALS R threads max_iters tolerance N dims[] s

This will create and decompose a tensor with rank, R tensor with 
1)	R, rank of the tensor and CP decomposition
2)	Use the given number of threads
3)	Maximum number of iterations CP_ALS will run
4)	Tolerance for determining when convergence is reached, -1.0 for machine eps or -2.0 for just run the maximum number of iterations.
5)	The number of dimensions in your tensor, must be at least 3
6)	The lengths of the dimensions
7)	S, the split point as denoted n_star in the cp_fastasl paper above.

Significance of this code.
This code was motivated by a project that wanted to computer CP decompositions of a 200 by 200 by 59 by 225 neuro-imaging tensor. 
Tobia, M. J., Hayashi, K., Ballard, G., Gotlib, I. H. and Waugh, C. E. (2017), Dynamic functional connectivity and individual differences in emotions during social stress. Hum. Brain Mapp.. doi:10.1002/hbm.23821
To obtain a rank 10, 10 iteration CP decomposition on the neuro-imaging tensor it takes

Sequential
Tensor Toolbox 117.77 seconds with cp_als
Tensor Box 55.8 seconds with fast_cpals()
This code 33.79 seconds with fast_CP_ALS()

12 thread parallel
Tensor Toolbox 25.36 seconds with cp_als()
Tensor Box 13.6 seconds with fast_cpals()
This code 4.66 seconds with fast_CP_ALS()
